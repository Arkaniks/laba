//
//  main.swift
//  laba1
//
//  Created by Владислав on 24.09.2021.
//

import Foundation

var arrayWithTerminal = [Int]()

repeat {
    print("Enter numbers separated by commas (,)")
    if let str = readLine() {
        arrayWithTerminal = str.components(separatedBy: ",").compactMap({Int($0)})
    }

} while arrayWithTerminal.count <= 2


var main = Main(array: arrayWithTerminal)

public class Main {
    private var array: [Int] = []
    private let run: Int = 32

    private func main() {
        timSort()
    }

    private func insertionSort(_ array:[Int]) -> [Int] {
        var a = array
        for x in 1..<a.count {
            var y = x
            while y > 0 && a[y] < a[y - 1] {
                a.swapAt(y - 1, y)
                y -= 1
            }
        }
        return a
    }

    init(array: [Int]) {
        self.array = array
        printTimeElapsedWhenRunningCode(title: "time:") {
            main()
        }
    }

    private func merge(leftPile: [Int], rightPile: [Int]) -> [Int] {
        var leftIndex = 0
        var rightIndex = 0

        var orderedPile = [Int]()

        while leftIndex < leftPile.count && rightIndex < rightPile.count {
            if leftPile[leftIndex] < rightPile[rightIndex] {
                orderedPile.append(leftPile[leftIndex])
                leftIndex += 1
            } else if leftPile[leftIndex] > rightPile[rightIndex] {
                orderedPile.append(rightPile[rightIndex])
                rightIndex += 1
            } else {
                orderedPile.append(leftPile[leftIndex])
                leftIndex += 1
                orderedPile.append(rightPile[rightIndex])
                rightIndex += 1
            }
        }

        while leftIndex < leftPile.count {
            orderedPile.append(leftPile[leftIndex])
            leftIndex += 1
        }

        while rightIndex < rightPile.count {
            orderedPile.append(rightPile[rightIndex])
            rightIndex += 1
        }

        return orderedPile
    }

    private func timSort() {
        print("Unsorted : \(array)")
        for i in stride(from: 0, to: array.count, by: run) {
            print("i : \(min((i + run),(array.count)))")
            array.replaceSubrange(i..<min((i + run),(array.count)), with: insertionSort(Array(array[i..<min((i + run),(array.count))])))
        }
        print("after insertion sort \(array)")

        var runCount = run
        while runCount < array.count{
            for x in stride(from: 0, to: array.count, by: 2 * runCount) {
                print("x : \(x) runCount \(runCount) calc : \(x + 2 * runCount)")
                array.replaceSubrange(x..<min(x + 2 * runCount, array.count), with: merge(leftPile: Array(array[x..<min(x + runCount, array.count)]), rightPile: Array(array[min(x + runCount, array.count)..<min(x + 2 * runCount, array.count)])))
            }
            runCount = runCount * 2
        }

        print("Sorted : \(array)")
    }

    //MARK: To check the elapsed time
    func printTimeElapsedWhenRunningCode(title:String, operation:()->()) {
        let startTime = CFAbsoluteTimeGetCurrent()
        operation()
        let timeElapsed = CFAbsoluteTimeGetCurrent() - startTime
        print("Time elapsed for \(title): \(timeElapsed) s.")
    }

    func timeElapsedInSecondsWhenRunningCode(operation: ()->()) -> Double {
        let startTime = CFAbsoluteTimeGetCurrent()
        operation()
        let timeElapsed = CFAbsoluteTimeGetCurrent() - startTime
        return Double(timeElapsed)
    }
}

extension Int {
    static func moc() -> [Int] {
        return [10,11,22,33,44,55,66,77,33,44,55,66,66,77,77,55,33,44,556,777,77,88,54,76,88,666,867,7875,3,4,54,4,5,6,7,35,53,6,4,5,6,73,5,3,4,5,6,73,5,6,3,5,63,5,6,3,6,3,5,6,3,5,64,6,4,5,6,3,5,6,35,64,64,64,64,64,6,6,46,4,5,67,3,6,5,35,6,3,56,35,6,3,56,3,56,35,63,6,3,5363,63,63,6,36,55]
    }
}
